/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setenv_2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguitar <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/21 16:03:32 by pguitar           #+#    #+#             */
/*   Updated: 2019/09/21 16:03:37 by pguitar          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minishell.h"

int	ft_valid_env(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (i != 0 && str[i] == '=' && str[i + 1] != '\0')
			return (i);
		i++;
	}
	return (0);
}

int	ft_strlen_env(char *str)
{
	int i;

	i = 0;
	while (str[i] != '=')
		i++;
	return (i);
}
