/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_function.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguitar <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/21 15:55:17 by pguitar           #+#    #+#             */
/*   Updated: 2019/09/21 15:55:29 by pguitar          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minishell.h"

int	ft_exit(void)
{
	return (0);
}

int	ft_env(char **args)
{
	int i;

	i = 0;
	if (args[1])
	{
		ft_putstr("env: ");
		ft_putstr(args[1]);
		ft_putstr(": No such file or directory\n");
		return (1);
	}
	if (g_env == NULL)
		ft_putstr("error environ");
	while (g_env[i])
	{
		ft_putstr(g_env[i++]);
		ft_putstr("\n");
	}
	return (1);
}

int	ft_unsetenv(char **args)
{
	int i;

	i = 0;
	if (args[2])
		ft_putstr("unsetenv: too many arguments\n");
	else if (args[1])
	{
		while (g_env[i])
		{
			if (ft_strncmp(g_env[i], args[1], ft_strlen_env(g_env[i])) == 0)
			{
				free(g_env[i]);
				g_len--;
				while (i < g_len)
				{
					g_env[i] = g_env[i + 1];
					i++;
				}
				g_env[i] = NULL;
				return (1);
			}
			i++;
		}
	}
	return (1);
}
