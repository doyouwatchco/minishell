/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguitar <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/21 15:56:10 by pguitar           #+#    #+#             */
/*   Updated: 2019/09/21 15:56:13 by pguitar          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minishell.h"

int		ft_is_env_exist_set(char *search, char *set)
{
	int		i;
	char	*tmp;

	i = 0;
	while (g_env[i])
	{
		if (ft_strncmp(g_env[i], search, ft_strlen(search)) == 0)
		{
			ft_strdel(&g_env[i]);
			tmp = ft_strjoin(search, "=");
			g_env[i] = ft_strjoin(tmp, set);
			free(tmp);
			return (1);
		}
		i++;
	}
	return (0);
}

void	ft_print_pth(char *path)
{
	ft_putstr(path);
	ft_putchar('\n');
}

void	ft_change_dir(char *path, int print_path)
{
	char	*cwd;
	char	buff[4097];

	cwd = getcwd(buff, 4096);
	if (!chdir(path))
	{
		if (print_path)
			ft_print_pth(path);
		ft_is_env_exist_set("OLDPWD", cwd);
		cwd = getcwd(buff, 4096);
		ft_is_env_exist_set("PWD", cwd);
	}
	else
	{
		ft_putstr("minishell: cd: ");
		if (access(path, F_OK) == -1)
			ft_putstr("no such file or directory: ");
		else if (access(path, R_OK) == -1)
			ft_putstr("permission denied: ");
		else
			ft_putstr("not a directory: ");
		ft_putstr(path);
		ft_putchar('\n');
	}
}

int		ft_cd(char **args)
{
	char	*home_path;

	home_path = ft_get_env_var("HOME");
	args++;
	if (!args[0])
	{
		if (args[1])
		{
			ft_putstr("minishell: cd: too many arguments\n");
			return (1);
		}
		ft_change_dir(home_path, 0);
		return (1);
	}
	if (ft_strequ(args[0], "--"))
	{
		ft_change_dir(home_path, 0);
		return (1);
	}
	else if (args[0][0] == '-' && !args[0][2])
	{
		ft_change_dir(ft_get_env_var("OLDPWD"), 1);
		return (1);
	}
	ft_change_dir(args[0], 0);
	return (1);
}
