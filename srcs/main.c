/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguitar <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/10 16:35:34 by pguitar           #+#    #+#             */
/*   Updated: 2019/08/10 16:35:35 by pguitar          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minishell.h"

//void	ft_free_tmp(char **tmp)
//{
//	int i;
//
//	i = 0;
//	while (tmp[i])
//		free(tmp[i++]);
//
//}
//
//char	*ft_tilda(char *token)
//{
//	int i;
//	char *tmp;
//
//	i = 0;
//	while (token[i])
//	{
//		if (token[i] == '~' && token[i + 1] != '~')
//		{
//			if (i == 0)
//				return (token = ft_strjoin(getenv("HOME"), ++token));
//			else if (i > 0)
//			{
//				token[i] = '\0';
//				tmp = ft_strjoin(token, getenv("HOME"));
//				token = ft_strjoin(tmp, (token + i + 1));
//				free(tmp);
//				return (token);
//			}
//		}
//		i++;
//	}
//	return (token);
//}
//
//char	*ft_tilda_dollar(int i, char *token)
//{
//	static int	j;
//	static char	*tmp[100];
//
//
//	if (i == 0)
//	{
//		if (ft_strchr(token, '~'))
//		{
//			token = ft_tilda(token);
//			tmp[j++] = token;
//		}
//		if (ft_strchr(token, '$'))
//		{
//			token = ft_search_env(token + 1);
//			tmp[j++] = token;
//		}
//		tmp[j] = NULL;
//	}
//	else if (i == 1)
//		ft_free_tmp(tmp);
//	return (token);
//}

char	*ft_tilda_doll(char *line)
{
	int i;
	char *tmp;
	char *tmp_line;

	i = 0;
	while (line[i])
	{
		if (i != 0 && line[i] == '~' && line[i - 1] == ' '
			&& (line[i + 1] == ' ' || line[i + 1] == '/' ||
			line[i + 1] == ';' || line[i + 1] == '\0'))
		{
			tmp_line = line;
			line[i] = '\0';
			tmp = ft_strjoin(line, getenv("HOME"));
			line = ft_strjoin(tmp, (line + i + 1));
			free(tmp_line);
			free(tmp);
		}
		i++;
	}

	return (line);
}

void	ft_split_line_1(char *token, int *i, int *position, char **tokens)
{
	while (token != NULL)
	{
		tokens[*position] = token;
		(*position)++;
		if (*i == 1)
			break ;
		token = ft_strtok_1(NULL, MSH_DELIM, i);
	}
}

int		ft_split_line(char *line, int *i)
{
	int		status;
	int		bufsize;
	int		position;
	char	*token;
	char	**tokens;

	position = 0;
	bufsize = MSH_BUFSIZE;
	if (line[0] == ';')
		return (ft_syntax_error());
	tokens = (char**)malloc(bufsize * sizeof(char*));
	if (!tokens)
	{
		ft_putstr("minishell: memory error\n");
		return (-1);
	}
	token = ft_strtok_1(line, MSH_DELIM, i);
	ft_split_line_1(token, i, &position, tokens);
	tokens[position] = NULL;
	status = ft_execution(tokens);
	free(tokens);
	if (*i == 1)
		return (ft_split_line(line, i));
	return (status);
}

int		ft_run_cmd(char *line)
{
	int i;
	int status;

	i = -1;
	status = 1;
	if ((status = ft_split_line(line, &i)) == -1)
		exit(EXIT_FAILURE);
//	ft_tilda_dollar(1, "");
	return (status);
}

void	ft_loop(void)
{
	int		status;
	char	*line;

	line = NULL;
	status = 1;
	ft_fill_env();
	while (status)
	{
		ft_fullpath();
		signal(SIGINT, signal_handler);
		get_next_line(0, &line);
		if (ft_home_directory(&line))
			continue;
		line = ft_tilda_doll(line);
		status = ft_run_cmd(line);
		free(line);
	}
}

int		main(void)
{
	ft_loop();
	ft_free_env();
	return (EXIT_SUCCESS);
}
