/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   support.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguitar <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/21 16:03:55 by pguitar           #+#    #+#             */
/*   Updated: 2019/09/21 16:03:59 by pguitar          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minishell.h"

char	*ft_pathjoin(char *path, char *args)
{
	char *str;
	char *tmp;

	tmp = ft_strjoin(path, "/");
	str = ft_strjoin(tmp, args);
	free(tmp);
	return (str);
}

int		ft_strstart(char *s1, char *s2)
{
	int i;

	i = -1;
	while (s2[++i])
		if (s1[i] != s2[i])
			return (0);
	return (1);
}

int		ft_home_directory(char **line)
{
	if (ft_strcmp("~", *line) == 0)
	{
		ft_putstr("minishell: ");
		ft_putstr(getenv("HOME"));
		ft_putstr(": is a directory\n");
		free(*line);
		return (1);
	}
	return (0);
}

void	ft_cmd_not_found(char *args)
{
	ft_putstr("minishell: ");
	ft_putstr(args);
	ft_putstr(": command not found\n");
}

int		ft_fullpath(void)
{
	char pathbuf[PATH_MAX + 1];

	getcwd(pathbuf, PATH_MAX + 1);
	ft_putstr("\033[36m");
	ft_putstr(pathbuf);
	ft_putstr(" \033[34m> ");
	ft_putstr("\033[0m");
	return (1);
}
