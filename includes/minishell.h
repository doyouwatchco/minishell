/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguitar <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/10 16:39:36 by pguitar           #+#    #+#             */
/*   Updated: 2019/09/10 13:21:41 by pguitar          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# define MSH_BUFSIZE 64
# define MSH_DELIM " \t\r\n\a;"
# define IS_QUOTE(x) (x == '"' || x == '\'')
# include "../libft/includes/libft.h"
# include <stdlib.h>
# include <sys/wait.h>
# include <stdio.h>
# include <dirent.h>
# include <limits.h>
# include <signal.h>
# include <sys/stat.h>

int		g_len;
char	**g_env;

int		ft_cd(char **args);
int		ft_exit();
int		ft_echo(char **args);
int		ft_env();
int		ft_setenv(char **args);
int		ft_unsetenv(char **args);
int		ft_valid_env(char *str);
int		ft_free_env();
int		ft_fullpath();
int		ft_home_directory(char **line);
void	ft_cmd_not_found(char *args);
void	proc_signal_handler(int signo);
void	signal_handler(int signo);
int		ft_fill_env(void);
char	*ft_search_env(char *token);
int		ft_check_bin(char **args);
int		ft_strlen_env(char *str);
int		ft_strstart(char *s1, char *s2);
char	*ft_pathjoin(char *path, char *args);
char	*ft_get_env_var(char *str);
int		ft_execution(char **args);
char	*ft_strtok_1(char *s, const char *delim, int *i);
int		ft_syntax_error(void);

#endif
