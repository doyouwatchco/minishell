/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_1.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguitar <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/21 17:38:10 by pguitar           #+#    #+#             */
/*   Updated: 2019/09/21 17:38:12 by pguitar          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minishell.h"

int		ft_syntax_error(void)
{
	ft_putstr("minishell: ");
	ft_putstr("\";\"");
	ft_putstr(": syntax error\n");
	return (1);
}

int		ft_builtin(char **args)
{
	if (!ft_strcmp(args[0], "cd"))
		return (ft_cd(args));
	else if (!ft_strcmp(args[0], "echo"))
		return (ft_echo(args));
	else if (!ft_strcmp(args[0], "exit") || !ft_strcmp(args[0], "EXIT"))
		return (ft_exit());
	else if (!ft_strcmp(args[0], "env"))
		return (ft_env(args));
	else if (!ft_strcmp(args[0], "setenv"))
		return (ft_setenv(args));
	else if (!ft_strcmp(args[0], "unsetenv"))
		return (ft_unsetenv(args));
	return (-1);
}

int		ft_execution(char **args)
{
	int j;

	if (args[0] == NULL)
	{
		ft_putstr("command not entered\n");
		return (1);
	}
	if ((j = ft_builtin(args)) != -1)
		return (j);
	ft_check_bin(args);
	return (1);
}

size_t	ft_strcspn_1(const char *s1, const char *s2, int *i)
{
	size_t ret;

	ret = 0;
	while (*s1)
	{
		if (ft_strchr(s2, *s1))
		{
			if (*s1 == ';')
				*i = 1;
			else
				*i = 0;
			return (ret);
		}
		else
		{
			s1++;
			ret++;
		}
	}
	return (ret);
}

char	*ft_strtok_1(char *s, const char *delim, int *i)
{
	int			ch;
	static char	*lasts;

	if (*i == -1)
		*i = 0;
	else if (s == 0 || (*i == 1 && lasts))
		s = lasts;
	if ((ch = *s++) == '\0')
		return (0);
	while (ft_strchr(delim, ch))
	{
		if (ch == ';')
		{
			*i = 1;
			lasts = lasts + 1;
			return (0);
		}
		if ((ch = *s++) == '\0')
			return (0);
	}
	--s;
	lasts = s + ft_strcspn_1(s, delim, i);
	if (*lasts != 0)
		*lasts++ = 0;
	return (s);
}
