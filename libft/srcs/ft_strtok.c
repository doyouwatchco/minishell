/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtok.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguitar <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/13 19:17:56 by pguitar           #+#    #+#             */
/*   Updated: 2019/08/13 19:17:57 by pguitar          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strtok(char *s, const char *delim)
{
	int ch;
	static char *lasts;

	if (s == 0)
		s = lasts;
	if ((ch = *s++) == '\0')
		return 0;
	while (ft_strchr(delim, ch))
	{
		if ((ch = *s++) == '\0')
			return 0;
	}
	--s;
	lasts = s + ft_strcspn(s, delim);
	if (*lasts != 0)
		*lasts++ = 0;
	return s;
}
