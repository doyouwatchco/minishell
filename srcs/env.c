/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguitar <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/21 15:57:05 by pguitar           #+#    #+#             */
/*   Updated: 2019/09/21 15:57:10 by pguitar          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minishell.h"

char	*ft_strcmp_env(char *s1, char *s2)
{
	while (*s1 == *s2)
	{
		s1++;
		s2++;
	}
	if (*s1 == '\0' && *s2 == '=')
		return (++s2);
	return (NULL);
}

char	*ft_search_env(char *token)
{
	int		i;
	char	*str;

	i = 0;
	while (g_env[i])
	{
		if ((str = ft_strcmp_env(token, g_env[i])))
			return (str);
		i++;
	}
	return (str);
}

int		ft_envlen(char **environ)
{
	int i;

	i = 0;
	while (environ[i])
		i++;
	g_len = i;
	return (i);
}

int		ft_fill_env(void)
{
	int			i;
	int			len;
	extern char	**environ;

	i = 0;
	len = ft_envlen(environ);
	g_env = (char **)malloc(sizeof(*g_env) * (len + 1));
	while (i < len)
	{
		g_env[i] = ft_strdup(environ[i]);
		i++;
	}
	g_env[i] = NULL;
	return (1);
}

int		ft_free_env(void)
{
	int i;

	i = 0;
	while (g_env[i])
	{
		free(g_env[i]);
		i++;
	}
	free(g_env);
	return (1);
}
