/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_and_launch.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguitar <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/21 15:56:33 by pguitar           #+#    #+#             */
/*   Updated: 2019/09/21 15:56:36 by pguitar          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minishell.h"

int		ft_launch(char *bin, char **args)
{
	pid_t	pid;
	pid_t	wpid;
	int		status;

	pid = fork();
	signal(SIGINT, proc_signal_handler);
	if (pid == 0)
	{
		if (execve(bin, args, g_env) == -1)
			ft_cmd_not_found(args[0]);
		return (2);
	}
	else if (pid < 0)
	{
		ft_putstr("minishell: error fork\n");
	}
	else
	{
		wpid = waitpid(pid, &status, WUNTRACED);
		while (!WIFEXITED(status) && !WIFSIGNALED(status))
			wpid = waitpid(pid, &status, WUNTRACED);
	}
	if (bin)
		free(bin);
	return (1);
}

char	*ft_get_env_var(char *str)
{
	int i;
	int j;

	i = 0;
	j = ft_strlen(str);
	while (g_env[i])
	{
		if (ft_strncmp(str, g_env[i], j) == 0 && g_env[i][j] == '=')
			return (g_env[i] + ft_strlen(str) + 1);
		i++;
	}
	return (0);
}

int		ft_check_exe(char *bin, struct stat f, char **args)
{
	if (f.st_mode & S_IFREG)
	{
		if (f.st_mode & S_IXUSR)
			return (ft_launch(bin, args));
		else
		{
			ft_putstr("minishell: permission denied: ");
			ft_putendl(bin);
		}
		free(bin);
		return (1);
	}
	free(bin);
	return (0);
}

int		ft_free(char **args)
{
	int i;

	i = 0;
	if (!args)
		return (0);
	while (args[i])
	{
		free(args[i]);
		i++;
	}
	free(args);
	args = NULL;
	return (1);
}

int		ft_check_bin(char **args)
{
	int			i;
	char		**path;
	char		*bin;
	struct stat	f;

	i = 0;
	path = ft_strsplit(ft_get_env_var("PATH"), ':');
	while (path && path[i])
	{
		if (ft_strstart(args[0], path[i]))
			bin = ft_strdup(args[0]);
		else
			bin = ft_pathjoin(path[i], args[0]);
		if (lstat(bin, &f) == -1)
			free(bin);
		else
		{
			ft_free(path);
			return (ft_check_exe(bin, f, args));
		}
		i++;
	}
	ft_free(path);
	ft_cmd_not_found(args[0]);
	return (1);
}
