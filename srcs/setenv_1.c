/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setenv_1.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguitar <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/21 15:57:31 by pguitar           #+#    #+#             */
/*   Updated: 2019/09/21 15:57:35 by pguitar          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minishell.h"

int	ft_is_env_exist_2(char **args)
{
	int i;
	int j;

	i = 0;
	if (!(j = ft_valid_env(args[1])))
		return (0);
	while (g_env[i])
	{
		if (ft_strncmp(g_env[i], args[1], j) == 0)
		{
			ft_strdel(&g_env[i]);
			g_env[i] = ft_strdup(args[1]);
			return (1);
		}
		i++;
	}
	return (0);
}

int	ft_is_env_exist_1(char **args)
{
	int		i;
	char	*tmp;

	i = 0;
	if ((!args[2]))
		return (ft_is_env_exist_2(args));
	while (g_env[i])
	{
		if (ft_strncmp(g_env[i], args[1], ft_strlen(args[1])) == 0)
		{
			ft_strdel(&g_env[i]);
			tmp = ft_strjoin(args[1], "=");
			g_env[i] = ft_strjoin(tmp, args[2]);
			free(tmp);
			return (1);
		}
		i++;
	}
	return (0);
}

int	ft_realloc_1(int new)
{
	int		i;
	char	**new_str;

	i = -1;
	new_str = (char **)malloc(sizeof(*new_str) * (new + 1));
	while (g_env[++i])
		new_str[i] = g_env[i];
	free(g_env);
	new_str[i + 1] = NULL;
	g_env = new_str;
	g_len = new;
	return (i);
}

int	ft_is_env_noexist(char **args)
{
	int		i;
	char	*tmp;

	i = ft_realloc_1(g_len + 1);
	if (args[2])
	{
		tmp = ft_strjoin(args[1], "=");
		g_env[i] = ft_strjoin(tmp, args[2]);
		free(tmp);
	}
	else
		g_env[i] = ft_strdup(args[1]);
	return (0);
}

int	ft_setenv(char **args)
{
	if (args[2] && args[3])
	{
		ft_putstr("setenv: too many arguments\n");
		return (1);
	}
	if (args[1])
	{
		if (!args[2])
		{
			if (!(ft_valid_env(args[1])))
			{
				ft_cmd_not_found(args[1]);
				return (1);
			}
		}
		if (!(ft_is_env_exist_1(args)))
			ft_is_env_noexist(args);
	}
	return (1);
}
